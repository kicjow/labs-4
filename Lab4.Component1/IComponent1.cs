﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Component2;
using Lab4.Contract;

namespace Lab4.Component1
{
    public class IComponent1 : AbstractComponent, Interface1
    {

        Interface1 InterfaceComponent1;

       public IComponent1(Interface1 interfacecom1)
        {
            this.InterfaceComponent1 = interfacecom1;
            RegisterProvidedInterface<Interface1>(this.InterfaceComponent1);

        }

       public IComponent1()
       {
           this.RegisterRequiredInterface<Interface1>();
       }

        public static Interface1 Test3()
        {
            return new IComponent1();
        }

        public override void InjectInterface(Type type, object impl)
        {
           // Console.WriteLine("asd");
        }

        public void Test1()
        {
            InterfaceComponent1.Test1();
        }

        public void Test2()
        {
            InterfaceComponent1.Test2();
        }
    }
}
