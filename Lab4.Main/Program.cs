﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Component1;
using Lab4.Component2;
using Lab4.Contract;
using ComponentFramework;


namespace Lab4.Main
{
    public class Program
    {
        static void Main(string[] args)
        {
            Container kont = new Container();

            IComponent2 komponent2 = new IComponent2();

            komponent2.RegisterProvidedInterface<Interface1>(komponent2);

            IComponent1 komponent1 = new IComponent1(kont.GetInterface<Interface1>());

            komponent1.Test1();
            
            
        }
    }
}
