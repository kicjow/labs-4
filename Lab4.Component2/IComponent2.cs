﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component2
{
    public class IComponent2 : AbstractComponent, Interface1
    {
        public void Test1()
        {
            Console.WriteLine("asd");
        }

        public void Test2()
        {
            Console.WriteLine("asd");
        }

        public IComponent2()
        {
            this.RegisterProvidedInterface(typeof(Interface1), this);
        }


        public override void InjectInterface(Type type, object impl)
        {
            Console.WriteLine("asds");
        }

        public static Interface1 test4()
        {

            return new IComponent2();
        }
    }
}
